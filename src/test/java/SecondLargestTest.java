import org.junit.Assert;
import org.junit.Test;


import static org.junit.Assert.*;

public class SecondLargestTest {

    private SecondLargest secondLargest= new SecondLargest();
    private Object ArrayContainsLessThenTwoUniqueElementsException;


    @Test
    public void findSecondLargestwithLargestAtTheEndOfArrayTest(){
        Assert.assertEquals((java.util.Optional.of(secondLargest.findSecondLargest(new int[]{5, 11, 3, 6, 8,19}))), java.util.Optional.of(11));
    }

    @Test
    public void findSecondLargestwithNothingSpecialArrayTest(){
        Assert.assertEquals((java.util.Optional.of(secondLargest.findSecondLargest(new int[]{19, 11, 3, 6, 8,19}))), java.util.Optional.of(11));
    }

    @Test(expected = NullPointerException.class)
    public void nullCheckTest(){
        Assert.assertEquals(new NullPointerException(),secondLargest.findSecondLargest(null));
    }

    @Test(expected = ArrayContainsLessThenTwoUniqueElementsException.class)
    public void lessThenTwoUniqueElementsTest(){
        Assert.assertEquals(ArrayContainsLessThenTwoUniqueElementsException,secondLargest.findSecondLargest(new int[]{5, 5, 5, 5, 5,5}));
    }

   


}