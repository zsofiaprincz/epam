import java.util.*;

public class SecondLargest {

    public static void main(String[] args) {

        int arr[] = {12,13,19,11,18,12,12,20};
        findSecondLargest(arr);
    }

        public static Integer findSecondLargest(int[] array){
        if (array == null) {
            throw  new NullPointerException();
            }
            Set<Integer> uniques = new HashSet<Integer>();
            for (int i=1; i<array.length; i++){
                uniques.add(array[i]);
            }

            if (uniques.size()<2){
                throw new ArrayContainsLessThenTwoUniqueElementsException("The array contains less then two unique elements");
            }
            int largestNumber = uniques.iterator().next();
            int secondLargestNumber = uniques.iterator().next();
            for (int i: uniques){
                if (i > largestNumber){
                    secondLargestNumber=largestNumber;
                    largestNumber=i;
                } else if (i> secondLargestNumber || secondLargestNumber==largestNumber){
                    secondLargestNumber=i;
                }
            }

            System.out.println(secondLargestNumber);
                return secondLargestNumber;
        }


    }







