public class ArrayContainsLessThenTwoUniqueElementsException extends RuntimeException {

    public ArrayContainsLessThenTwoUniqueElementsException(String msg){
        super(msg);
    }
}
